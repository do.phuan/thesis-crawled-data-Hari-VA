BEGIN{
    nbPages=0
}

/<h4 name="results-count">/{
    gsub(/<[^>]+>/,"",$0)
    gsub(/[^0-9]/,"",$0)
    nbPages=$0
}
END {
    print nbPages
}