BEGIN {
    i=1
    title[i]="PRODUCT_ID"; i++
    title[i]="PRODUCT_NAME"; i++
    title[i]="BRANCH"; i++
    title[i]="LINK"; i++
    title[i]="PRICE"; i++
    title[i]="TYPE"; i++
    title[i]="COLOR"; i++
    title[i]="PHOTO"; i++
    title[i]="RAM"; i++
    title[i]="SOC"; i++
    title[i]="STORAGE"; i++
    title[i]="SCREEN"; i++
    title[i]="SCREEN_RESOLUTION"; i++
    title[i]="CAMERA"; i++
    title[i]="OS"; i++
    title[i]="UTILITIES"; i++
    title[i]="WEIGHT"; i++
    title[i]="BATTERY"; i++
    title[i]="CONNECTION"; i++
    title[i]="RATING"; i++
    title[i]="SIZE"; i++
    max_i=i;
}