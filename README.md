# Data crawler 

This project is to create data for my graduation thesis by crawling data from TIKI.VN. 

## Getting started

This instruction is to describe required steps and directory structure of the crawling system which was written in Shell script and awk. 

## Rrerequisite
- Read this doc carefully. 
- Know some basic git command eg: push, commit, add, create new branch, switch to another branch and etc **(MUST)**
- Always push on Development branch _(created already)_.
- Temp file must be removed before commiting and pushing to git. 

## Basic git command 

- Clone to existed folder: 

```shell
$ cd your_existing_folder
$ git init
$ git remote add origin git@gitlab.com:**your_git_username**/thesis-crawled-data-Hari-VA.git
$ git checkout development
$ git add .
$ git commit -m "Init commit"
$ git push -u origin development
```

## Repository Structure 

- **Working directory**: current dir in which download_site.sh and list_mode pages are saved.
- **ALL**: Place to store all _detail_ pages. 
- **DELTA**: Save all parsing results.  

## Notes: 

Because we have to parse various item types so **PLEASE** create a new folder in current working directory (the folder in which code is cloned into).

Update: The result will be saved in csv format file with utf-8 encoding. 

When we echo a unicode word or a short paragraph, the text file will be convert to utf-8 automatically. 

## Program structure:

- **download_site.sh:** Main code file to download all required pages. 
- **put_html_into_tab.awk:** Parse list mode pages to extract.tab 
- **put_tab_into_db.awk:** put all data in extract.tab to sql file
- **extract.tab:** file in which data is parsed. 
- **parsing_details.awk:** Extract all details in detail pages. 
- **status_ok:** the flag file which let us know if our code is successfully downloaded. 
- **insert.sql and update.sql:** deliverable files

## How to run?
Change directory to the path containing `download_site.sh`

Then type: 

> ./download_site.sh `<options>` -l`<link to be downloaded>` -d`<path to save data>`

Example:

```shell
./download_site.sh -p -l"https://tiki.vn/laptop/" -d"Laptop_site"
```

## Options: 
**-a:** download and parse all available data. 

**-t:** test mode. In this mode, there only the page number 1 is downloaded. 

**-p:** parse only mode. Web won't be donwloaded. 