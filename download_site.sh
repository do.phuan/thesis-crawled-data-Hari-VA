#!/bin/bash 

# -------------------------------------------------------------------------------
# Initialization 
# -------------------------------------------------------------------------------
work_dir=`pwd`
day=`date +"%Y%m%d"`

. ${work_dir}/list_useragent.sh
u_a=0 


maxLoops=5
loop=0
page=0
TAB="   "
commonLink=""

downloadAll=false 
testMode=false
parseDataOnly=false

while getopts "atpd:l:" opts; do 
    case ${opts} in 
        l)
            commonLink="${OPTARG}"
        ;;
        d)  
            day="${OPTARG}"            
        ;;
        a)
            downloadAll=true
        ;;
        t)
            testMode=true
        ;;
        p)
            parseDataOnly=true 
        ;;
    esac
done

mkdir -p ${work_dir}/${day}/DELTA ${work_dir}/${day}/ALL


# -------------------------------------------------------------------------------
# Required Functions 
# -------------------------------------------------------------------------------
function downloadListModePage {
    loop=0
    page=$1
    link=$2

    while [ ${loop} -lt ${maxLoops} ]
    do 
        if [ ! -s ${work_dir}/${day}/page-${page}.html ]
        then 
            wget -c -nv --timeout=15 --tries=5 --waitretry=1 -U"${USERAGENT_ARR[$u_a]}" --random-wait --keep-session-cookies --save-cookies=${work_dir}/${day}/cookies.$$ ${link} -O ${work_dir}/${day}/page-${page}.html
        fi 

        # Check if the file was downloaded successfully
        grep -i "<\/html>" ${work_dir}/${day}/page-${page}.html
        if [ $? -ne 0 ]
        then
            rm -f ${work_dir}/${day}/page-${page}.html
            let "loop=loop+1"
        else
            loop=${maxLoops}
            echo "Download page $page successfully."
        fi
    done 
}

function downloadDetailPages {
    id=$1
    link=$2
    if [ ! -s ${work_dir}/${day}/ALL/page-${id}.html ]
    then 
        wget -c -nv --timeout=15 --tries=5 --waitretry=1 -U"${USERAGENT_ARR[$u_a]}" --random-wait --keep-session-cookies --save-cookies=${work_dir}/${day}/cookies.$$ ${link} -O ${work_dir}/${day}/ALL/page-${id}.html
    fi 
}

function downloadAndParseDetailedInfo 
{
    rm -rf ${work_dir}/${day}/DELTA/update.sql ${work_dir}/${day}/DELTA/insert.sql
    
    while read id link
    do 
        if [ "${parseDataOnly}" = false ]
        then 
            echo "Start download detail pages..."
            downloadDetailPages $id $link 
        fi 
        awk -f ${work_dir}/parsing_details.awk ${work_dir}/${day}/ALL/page-${id}.html >> ${work_dir}/${day}/DELTA/update.sql 
    done < ${work_dir}/${day}/extract.tab

    if [ -s ${work_dir}/${day}/DELTA/update.sql ]
    then 
        awk -f ${work_dir}/tabs_insert.awk -f ${work_dir}/put_tab_into_db.awk ${work_dir}/${day}/extract.tab > ${work_dir}/${day}/DELTA/insert.sql
        echo "OK" > ${work_dir}/${day}/DELTA/status_ok
    fi 
}

function getAllDataModeOn {
    downloadListModePage 1 ${commonLink}
    
    nbPages2Download=0 
    nbAdPerPage=24
    
    nb_pages=`awk -f ${work_dir}/get_number_pages.awk ${work_dir}/${day}/page-1.html`
    let "nbPages2Download=nb_pages / nbAdPerPage"
    let "mod=nb_pages % nbAdPerPage"
    
    if [ ${mod} -gt 0 ]
    then
        let "nbPages2Download=nbPages2Download+1"
    fi

    page=1
    while [ ${page} -le ${nbPages2Download} ]
    do
        let "page=page+1"
        downloadListModePage ${page} ${commonLink}"&page=${page}"
    done 

    page=1
    while [ ${page} -lt ${nbPages2Download} ]
    do
        awk -f ${work_dir}/put_html_into_tab.awk ${work_dir}/${day}/page-${page}.html >> ${work_dir}/${day}/extract.$$
        let "page=page+1"
    done 
    
    sort -u ${work_dir}/${day}/extract.$$ > ${work_dir}/${day}/extract.tab
    rm -rf ${work_dir}/${day}/extract.$$

    downloadAndParseDetailedInfo 
}

function getDataTestModeOn {
    downloadListModePage 1 ${commonLink}

    awk -f ${work_dir}/put_html_into_tab.awk ${work_dir}/${day}/page-${page}.html >> ${work_dir}/${day}/extract.$$
    sort -u ${work_dir}/${day}/extract.$$ > ${work_dir}/${day}/extract.tab
    rm -rf ${work_dir}/${day}/extract.$$

    downloadAndParseDetailedInfo
}
# -------------------------------------------------------------------------------
# Main program 
# -------------------------------------------------------------------------------
if ${downloadAll}
then
    echo "Download all mode is on"
    getAllDataModeOn   
fi 

if ${testMode}
then 
    echo "Test mode is on"
    getDataTestModeOn
fi 

if [ "${parseDataOnly}" = true ]
then 
    echo "Parse data only mode is on"
    if [ -s ${work_dir}/${day}/extract.tab ]
    then 
        downloadAndParseDetailedInfo    
    else 
        echo "Fail to parse data due to the missing of extract.tab"
    fi 
fi 